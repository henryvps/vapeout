<?php
/**
* Plugin Name: VapeShop CPT
* Plugin URI: http://vapeout.com
* Version: 1.0
* Author: Markkus Millend
* Author URI: http://vapeout.com
* Description: VapeShop CPT 
* License: GPL2
*/

class VapeShopCPT {

	function __construct() {
		add_action('init', array(&$this, 'register_custom_post_type'));
		add_action('add_meta_boxes', array(&$this, 'register_meta_boxes'));
		add_action('save_post', array(&$this, 'save_meta_boxes'));
        add_action('bp_core_signup_user', array(&$this, 'register_new_user'));
        add_filter('post_type_link', array(&$this, 'rewrite_vapeshop_to_groups'));
	}



    function rewrite_vapeshop_to_groups() {
        $args = array(
            'post_type' => 'vapeshop'
        );
        $vapeshops = get_posts( $args );

        foreach($vapeshops as $vs) {
            if ($vs->post_type == 'vapeshop'){
                $group = groups_get_group(array('group_id' => $vs->group_id));
                return home_url(bp_get_groups_root_slug() . '/' . $group->slug . '/');
            } else {
                return home_url('#');
            }
        }


    }

    function register_new_user($user_id, $username, $password, $email, $meta) {
        // This is dirty.
        // Should check if we are adding vape shop or a community user.
        if (isset( $_POST['field_6']) || isset( $_POST['signup_usertype']))
            $this->create_vapeshop($user_id, $_POST['vapeshop_name'], $_POST['vapeshop_address'], $_POST['vapeshop_lat'], $_POST['vapeshop_lng']);

    }


    function create_vapeshop($user_id, $vapeshop_name, $address, $lat, $lng) {
        // Let's create vapeshop post
        $vapeshop_id = $this->create_vapeshop_post($user_id, $vapeshop_name, $address, $lat, $lng);

        // Create a new Buddypress Group
        $group = $this->create_group($user_id, $vapeshop_name);

        // Update group meta so it would show up in frontend
        $this->update_group_meta($group);

        // Connect vapeshop with a group
        $this->vapeshop_addgroup($vapeshop_id, $group);

        // Update


    }

    function create_vapeshop_post($user_id, $vapeshop_name, $address, $lat, $lng) {

        $post = array(
                'comment_status'	=>	'closed',
                'ping_status'		=>	'closed',
                'post_author'		=>	$user_id,
                'post_name'		    =>	sanitize_title($vapeshop_name),
                'post_title'		=>	$vapeshop_name,
                'post_status'		=>	'publish',
                'post_type'		    =>	'vapeshop'
        );

        // Create initial vapeshop post
        $post_id = wp_insert_post($post);

        // Add vapeshop location to the post as meta
        if($post_id) {
//            add_post_meta($post_id, '_pronamic_google_maps_active', 'true', true);
//            add_post_meta($post_id, '_pronamic_google_maps_address', $address, true);
//            add_post_meta($post_id, '_pronamic_google_maps_geocode_status', 'OK', true);
            add_post_meta($post_id, '_vapeshop_desc', $vapeshop_name, true);
            add_post_meta($post_id, '_wp_geo_title', $vapeshop_name, true);
            add_post_meta($post_id, '_wp_geo_latitude', $lat, true);
            add_post_meta($post_id, '_wp_geo_longitude', $lng, true);
        }

        return $post_id;
    }

    function create_group($user_id, $group_name) {

        $new_group = new BP_Groups_Group;

        $new_group->creator_id = $user_id;
        $new_group->name = $group_name;
        $new_group->slug = sanitize_title($group_name);
        $new_group->description = 'Vapeshop default description.';
        $new_group->news = 'whatever';
        $new_group->status = 'public';
        $new_group->is_invitation_only = 1;
        $new_group->enable_wire = 1;
        $new_group->enable_forum = 1;
        $new_group->enable_photos = 1;
        $new_group->photos_admin_only = 1;
        $new_group->date_created = current_time('mysql');
        $new_group->total_member_count = 1;

        $new_group -> save();

        return $new_group;

    }

    function update_group_meta($group) {
        $id = $group->id;
        groups_update_groupmeta( $id, 'total_member_count', 1 );
        groups_update_groupmeta( $id, 'last_activity', time() );
        groups_update_groupmeta( $id, 'theme', 'buddypress' );
        groups_update_groupmeta( $id, 'stylesheet', 'buddypress' );
        echo "<br />";
    }

    function vapeshop_addgroup($vapeshop_id, $group) {
        update_post_meta($vapeshop_id, 'group_id', $group->id);
    }


    function create_placemark() {

    }



	function register_custom_post_type() {
		register_post_type('vapeshop', array(
            'labels' => array(
				'name'               => _x('VapeShops', 'post type general name', 'vs-cpt'),
				'singular_name'      => _x('VapeShop', 'post type singular name', 'vs-cpt'),
				'menu_name'          => _x('VapeShops', 'admin menu', 'vs-cpt'),
				'name_admin_bar'     => _x('VapeShop', 'add new on admin bar', 'vs-cpt'),
				'add_new'            => _x('Add New', 'vapeshop', 'vs-cpt'),
				'add_new_item'       => __('Add New VapeShop', 'vs-cpt'),
				'new_item'           => __('New VapeShop', 'vs-cpt'),
				'edit_item'          => __('Edit VapeShop', 'vs-cpt'),
				'view_item'          => __('View VapeShop', 'vs-cpt'),
				'all_items'          => __('All VapeShops', 'vs-cpt'),
				'search_items'       => __('Search VapeShops', 'vs-cpt'),
				'parent_item_colon'  => __('Parent VapeShops:', 'vs-cpt'),
				'not_found'          => __('No VapeShops found.', 'vs-cpt'),
				'not_found_in_trash' => __('No VapeShops found in Trash.', 'vs-cpt'),
			),
            
            // Frontend
            'has_archive' => false,
            'public' => true,
            'publicly_queryable' => false,
            
            // Admin
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-location-alt',
            'menu_position' => 10,
            'query_var' => true,
            'show_in_menu' => true,
            'show_ui' => true,
            'supports' => array(
            	'title',
            	'author',
            	'comments',
            ),
        ));	
	}
	
	function register_meta_boxes() {
		add_meta_box('vapeshop-details', __('VapeShop Details', 'vs-cpt'), array(&$this, 'output_meta_box'), 'vapeshop', 'normal', 'high');	
	}

	function output_meta_box($post) {
		// Get email address
		$email = get_post_meta($post->ID, '_vapeshop_email', true);
		$desc = get_post_meta($post->ID, '_vapeshop_desc', true);
	
		// Add a nonce field so we can check for it later.
		wp_nonce_field('save_vapeshop', 'vapeshops_nonce');
		// Output labels and fields
		// Ugliest table in the world -- Definitely needs some revision
		echo '<table class="form-table"><tbody><tr><th scope="row">';	
		echo ('<label for="vapeshop_email">' . __('Email Address', 'vs-cpt') . '</label>');		
		echo '</th><td>';
		echo ('<input type="text" name="vapeshop_email" id="vapeshop_email" value="'.esc_attr($email).'" />');
		echo '</td></tr><tr><th scope="row">';
		echo ('<label for="vapeshop_desc">' . __('Description', 'vs-cpt') . '</label>');	
		echo '</th><td>';
		echo ('<textarea name="vapeshop_desc" id="vapeshop_desc" rows="4" cols="50">'.esc_attr($desc).'</textarea>');
		echo '</td></tr></tbody></table>';

        $group_id = get_post_meta($post->ID, 'group_id', true);

		$groups = BP_Groups_Group::get(array('type'=>'alphabetical', 'per_page'=>999));
		$groupcount = count($groups);
		for ($i = 0; $i < $groupcount; $i++) {
            if ($group_id == $groups['groups'][$i]->id) {
                echo 'Group id is <b>'.$groups['groups'][$i]->id.'</b> and the corresponding group name is <b>'.$groups['groups'][$i]->name.'</b><br>';
            }

		}

	}
	
	function save_meta_boxes($post_id) {
		// Check if our nonce is set.
		if (!isset($_POST['vapeshops_nonce'])) {
			return $post_id;	
		}
	
		// Verify that the nonce is valid.
		if (!wp_verify_nonce($_POST['vapeshops_nonce'], 'save_vapeshop')) {
			return $post_id;
		}
	
		// Check this is the vapeshop Custom Post Type
		if ('vapeshop' != $_POST['post_type']) {
			return $post_id;
		}
	
		// Check the logged in user has permission to edit this post
		if (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}
	
		// OK to save meta data
		$email = sanitize_text_field($_POST['vapeshop_email']);
		$desc = sanitize_text_field($_POST['vapeshop_desc']);
		// Perform updates
		update_post_meta($post_id, '_vapeshop_email', $email);
		update_post_meta($post_id, '_vapeshop_desc', $desc);
	}
}

$vscpt = new VapeShopCPT;